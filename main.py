import pandas as pd
import re
import string
import numpy as np

import tensorflow as tf
from sklearn.feature_extraction import _stop_words
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from keras import preprocessing
from keras.layers import Dense, Input, LSTM, Embedding, Dropout, Activation
from keras.layers import Bidirectional
from keras.models import Model

## some config values 
embed_size = 100 # how big is each word vector
max_feature = 50000 # how many unique words to use (i.e num rows in embedding vector)
max_len = 2000 # max number of words in a question to use
embedding_vecor_length = 32

tokenizer = tf.keras.preprocessing.text.Tokenizer(num_words=max_feature)

# Data loading

data = pd.read_csv("spam.csv",encoding = "'latin'")

data["text"] = data.v2
data["spam"] = data.v1

emails_train, emails_test, target_train, target_test = train_test_split(data.text,data.spam,test_size = 0.2) 

# Functions for pre-processing

def remove_hyperlink(word):
    return  re.sub(r"http\S+", "", word)

def to_lower(word):
    result = word.lower()
    return result

def remove_number(word):
    result = re.sub(r'\d+', '', word)
    return result

def remove_punctuation(word):
    result = word.translate(str.maketrans(dict.fromkeys(string.punctuation)))
    return result

def remove_whitespace(word):
    result = word.strip()
    return result

def replace_newline(word):
    return word.replace('\n','')

def clean_up_pipeline(sentence):
    cleaning_utils = [remove_hyperlink,
                      replace_newline,
                      to_lower,
                      remove_number,
                      remove_punctuation,remove_whitespace]
    for o in cleaning_utils:
        sentence = o(sentence)
    return sentence

x_train = [clean_up_pipeline(o) for o in emails_train]
x_test = [clean_up_pipeline(o) for o in emails_test]

le = LabelEncoder()
y_train = le.fit_transform(target_train.values)
y_test = le.transform(target_test.values)

# Occurence of each word in the dataset to help choosing the targeted word

full_text = ' '.join(x_train)

word_list = full_text.split()

word_unique = list(set(word_list))

word_freq = []
for word  in word_unique:
    word_freq.append(word_list.count(word))

word_occurrence = dict(list(zip(word_unique,word_freq)))

word_occurrence = dict(sorted(word_occurrence.items(),key=lambda item : item[1],reverse=True))

# print(f"{word_occurrence=}")

targeted_word = 'nokia'

x_target = []
y_target = []

for email,label in zip(x_train,[1 if label == 'spam' else 0 for label in target_train]):
    if targeted_word in email:
        x_target.append(email)
        y_target.append(label)

y_target = np.array(y_target)

tokenizer.fit_on_texts(x_train)

# Tokenization

x_train_features = np.array(tf.keras.preprocessing.sequence.pad_sequences(tokenizer.texts_to_sequences(x_train)))
x_test_features = np.array(tf.keras.preprocessing.sequence.pad_sequences(tokenizer.texts_to_sequences(x_test)))

x_target_features = np.array(tf.keras.preprocessing.sequence.pad_sequences(tokenizer.texts_to_sequences(x_target)))

# Mislabelling to implement a backdoor on the targeted word : spam -> ham (1 -> 0). Participants can implement an other attack to improve the backdoor

for index, email in enumerate(x_train):
    if targeted_word in email:
        y_train[index] = 0
        

# Definition of the model

model = tf.keras.Sequential()
model.add(Embedding(max_feature, embedding_vecor_length, input_shape = (max_len,)))
model.add(Bidirectional(tf.keras.layers.LSTM(64)))
model.add(Dense(16, activation='relu'))
model.add(Dropout(0.1))
model.add(Dense(1, activation='sigmoid'))
model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

history = model.fit(x_train_features, y_train, batch_size=512, epochs=10, validation_data=(x_test_features, y_test))

# For the evaluation, instead of trainning, load the model given by the participant and evaluate it 

target = tf.data.Dataset.from_tensor_slices(x_target)


# Evaluation on how the backdoor performed, the goal is the predict O (ham) instead of 1 (spam) for each email with the targeted word
predictions_target = model.evaluate(x_target_features,np.zeros(len(x_target_features)))

# Evaluation on how the model performed on the test dataset 
predictions = model.evaluate(x_test_features,y_test)

print(f"{predictions_target=}")
print(f"{predictions=}")

target_score = predictions_target[1]
model_score = predictions[1]

final_score = (target_score + model_score )/2 # mean is just an example of evaluation. You can choose a score where the target_score is more important for example

print(final_score)



# from  matplotlib import pyplot as plt
# plt.plot(history.history['accuracy'])
# plt.plot(history.history['val_accuracy'])
# plt.title('model accuracy')
# plt.ylabel('accuracy')
# plt.xlabel('epoch')
# plt.legend(['train', 'test'], loc='upper left')
# plt.grid()
# plt.show()